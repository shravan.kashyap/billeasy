import "./App.css";
import Gallary from "./Components/Gallary";
import React from "react";
import axios from "axios";
function App() {
  const [gallaryImages, setGallaryImage] = React.useState([]);
  const [width, setWidth] = React.useState(window.innerWidth);

  React.useEffect(() => {
    window.addEventListener("resize", updateWidthAndHeight);
    return () => window.removeEventListener("resize", updateWidthAndHeight);
  });

  const updateWidthAndHeight = () => {
    setWidth(window.innerWidth);
  };

  React.useEffect(() => {
    axios
      .get("https://www.mocky.io/v2/5ecb5c353000008f00ddd5a0")
      .then(function (res) {
        console.log(res.data);
        if (res.data.length > 0) {
          setGallaryImage(res.data);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return <Gallary gallaryImages={gallaryImages} width={width}></Gallary>;
}

export default App;
