import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ImageList from "@material-ui/core/ImageList";
import ImageListItem from "@material-ui/core/ImageListItem";
import { Container, ImageListItemBar } from "@material-ui/core";
import FullViewImage from "./FullViewImage";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    paddingTop: 20,
    paddingBottom: 20,
  },
}));

export default function Gallary(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [imageUrl, setImageUrl] = React.useState(false);

  const handleOpen = (Url) => {
    setImageUrl(Url);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Container>
      <div className={classes.root}>
        <ImageList
          cols={
            props.width < 500
              ? 1
              : props.width < 800
              ? 2
              : props.width < 1000
              ? 3
              : 4
          }
        >
          {props.gallaryImages.length > 0
            ? props.gallaryImages.map((item, ind) => (
                <ImageListItem
                  key={ind}
                  cols={item.cols || 1}
                  onClick={(e) => {
                    e.preventDefault();
                    handleOpen(item.urls.full);
                  }}
                >
                  <img
                    src={item.urls.regular}
                    alt={item.title}
                    style={{ objectFit: "cover", cursor: "pointer" }}
                  />
                  <ImageListItemBar title={item.alt_description} />
                </ImageListItem>
              ))
            : ""}
        </ImageList>
      </div>
      <FullViewImage
        handleClose={handleClose}
        open={open}
        imageUrl={imageUrl}
      ></FullViewImage>
    </Container>
  );
}
