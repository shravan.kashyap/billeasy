import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import { Fade } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "transparent",
    border: "5px ​solid blue",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(1),
    paddingBottom: 3,
  },
}));

export default function FullViewImage(props) {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={classes.modal}
        open={props.open}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={props.open}>
          <div className={classes.paper}>
            <img
              src={props.imageUrl}
              alt={".."}
              style={{
                objectFit: "cover",
                cursor: "pointer",
                width: 700,
                height: 600,
              }}
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
